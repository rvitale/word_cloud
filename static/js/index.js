
function render(words) {
    var canvas = document.getElementById('cloud-canvas');
    var devicePixelRatio = 1;
    // Trick needed for HiDpi screens.
    if (('devicePixelRatio' in window) && window.devicePixelRatio !== 1) {
        devicePixelRatio = window.devicePixelRatio;
    }
    var options = {
        gridSize: Math.round(16 * canvas.width / 1024) * devicePixelRatio,
        weightFactor: function (size) {
            return 2 * size * canvas.width / 1024 * devicePixelRatio;
        },
        rotateRatio: 0.5,
        list: words
    };
    WordCloud(canvas, options);
}

function process() {
    var urlBox = document.getElementById("url-box");
    var url = urlBox.value.replace(/ /g,'')
    if (url !== "") {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
                // TODO(riccardo): Error check.
                render(JSON.parse(xmlHttp.responseText));
        };
        xmlHttp.open("GET", "/search?url=" + url, true);
        xmlHttp.send(null);
    }
}

var submitBtn = document.getElementById('submit-btn');
submitBtn.onclick = process;
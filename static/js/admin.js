
window.onload = function() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
        // TODO(riccardo): Error check.
            render(JSON.parse(xmlHttp.responseText));
    };
    xmlHttp.open("GET", "/words", true);
    xmlHttp.send(null);
};

function render(words) {
    var content = document.getElementById("words-body");
    for (var i = 0; i < words.length; i++) {
        var tr = document.createElement('tr');
        var wordTd = document.createElement('td');
        wordTd.innerHTML = words[i][0];
        var countTd = document.createElement('td');
        countTd.innerHTML = words[i][1];
        tr.appendChild(wordTd);
        tr.appendChild(countTd);
        content.appendChild(tr);
    }
}
**Word Cloud**
********

The application is a Tornado web server serving the following routes:
- /search : to perform a search. It's a GET endpoint and it accepts the `url` parameter in order to pass the url to fetch and process.
- /words: to retrieve the word counts in the db. It's a GET endpoint and it returns a list of pairs of words and counts.
- /status: to perform a health check and see if the server is running.

When a URL is fetched, the text is stripped of the HTML and only the text is returned. The text is then normalized to lower case and stop words are removed too.

The following points are known things to improve with more time:
- the main page is at /static/index.html and I couldn't manage to redirect GAE to display it if / was requested. Not a big deal in my opinion for a prototype, so I deemed that other features would have been more important.
- the SQL insertion statement seems to work fine if I execute it twice, after which it seems to ignore the update. Might be something stupid, but I think it requires a bit of time to have a proper look at it.
- the salted hashing and encryption mechanism might be improved by respectively iteratively salted hashing the original key and having a better look at the asymmetric encryption.
- frontend is quite ugly.
- some more logging here and there.

In a real production environment, I would probably store the secrets in a secure store handled by a Puppet agent that deploys the secrets to the servers. 
This is a quite standard way of shipping secrets as well, as well as a maintainable way of updating the servers in case of the compromise of one or more of them (since the servers would be updated automatically).
In that case, though, there is the need to figure out a way of saving the database that has been populated with secrets that are not secrets anymore.



**Installation instructions**
********
Due to restriction in GAE, we need to vendorise Tornado in a local folder. We can do this by doing:

`pip install -t lib/ tornado`

The other requirements are listed in the requirements.txt:

`pip install -r requirements.txt`

Please note that for local development you will need a MySQL instance running and the Google App Engine SDK.
In order to successfully run the application, you will need to also generate a salt key and a PEM keypair file. Examples are provided in the folder (remove the *.example extension and replace the contents with the actual keys).
create database word_cloud;

use word_cloud;

create table words(hash VARCHAR(255) NOT NULL, word VARCHAR(255) NOT NULL, count integer, PRIMARY KEY (hash));
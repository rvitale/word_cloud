import hashlib
import os
import re
import urllib2
from ConfigParser import ConfigParser
from collections import Counter

import MySQLdb
from Crypto.PublicKey import RSA

import octo_parser

# TODO(riccardo): for some reason, it doesn't update if we request the same page more than twice.
INSERT_SQL = """INSERT INTO words VALUES(%s, %s, %s) ON DUPLICATE KEY UPDATE count=values(count)+%s"""
SELECT_SQL = """SELECT word, count FROM words ORDER BY count DESC"""

VALID_PATTERN = r"^[a-z'\-]{2,}$"


class Backend(object):
    """ Backend logic lives here. """

    # Limit of results to return.
    limit = 100

    _config_path = 'config.cfg'
    _salt_path = 'key.salt'
    _pem_path = 'key.pem'
    _stopwords_path = 'stopwords.txt'

    def __init__(self):
        with open(self._pem_path) as f:
            self.key = RSA.importKey(f)
        self.config = ConfigParser()
        self.config.read(self._config_path)
        with open(self._salt_path) as f:
            self.salt = f.read()
        with open(self._stopwords_path) as f:
            self.stopwords = {word.strip() for word in f}

    def _get_db_connection(self):
        if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine/'):
            return MySQLdb.connect(unix_socket='/cloudsql/' + self.config.get('db', 'socket_name'),
                                   user=self.config.get('db', 'username'),
                                   passwd=self.config.get('db', 'password'),
                                   db=self.config.get('db', 'name')
                                   )
        else:
            return MySQLdb.connect(host='localhost',
                                   user=self.config.get('db', 'username'),
                                   passwd=self.config.get('db', 'password'),
                                   db=self.config.get('db', 'name')
                                   )

    def search(self, url):
        """ Searches a url for valid words and extracts the most frequent words from it. """
        resp = urllib2.urlopen(url)
        if resp.code == 200:
            text = octo_parser.extract_text(resp.read())
            word_counts = self._tokenize_and_count(text)
            frequencies = word_counts.most_common(self.limit)
            self._register_frequencies(frequencies)
            return frequencies
        else:
            return []

    def get_words(self):
        """ Returns a list of the words found so far, in decreasing order"""
        conn = self._get_db_connection()
        try:
            cursor = conn.cursor()
            cursor.execute(SELECT_SQL)
            data = cursor.fetchall()
            return [(self._decrypt(result), count) for result, count in data]
        finally:
            conn.close()

    def _register_frequencies(self, frequencies):
        conn = self._get_db_connection()
        try:
            conn.autocommit = False
            cursor = conn.cursor()
            for pair in frequencies:
                cursor.execute(INSERT_SQL, (self._salt_hash(pair[0]), self._encrypt(pair[0]), pair[1], pair[1]))
            conn.commit()
        finally:
            conn.close()

    def _tokenize_and_count(self, text):
        counter = Counter()
        for word in text.lower().split():
            if word not in self.stopwords and re.match(VALID_PATTERN, word):
                counter[word] += 1
        return counter

    def _salt_hash(self, word):
        key = word + self.salt
        return hashlib.sha512(key).hexdigest()

    def _encrypt(self, word):
        return self.key.publickey().encrypt(word, 32)

    def _decrypt(self, message):
        return self.key.decrypt(message)

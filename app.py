import json
import logging
import urllib

from tornado import web, wsgi

from backend import Backend


class SearchHandler(web.RequestHandler):
    """ Handler for the /search endpoint. """

    def initialize(self, **kwargs):
        self.backend = kwargs.get('backend')

    def get(self):
        if self.get_argument('url', False):
            url = urllib.unquote(self.get_argument('url'))
            self.write(json.dumps(self.backend.search(url)))


class WordsHandler(web.RequestHandler):
    """ Handler for the /words endpoint. """

    def initialize(self, **kwargs):
        self.backend = kwargs.get('backend')

    def get(self):
        self.write(json.dumps(self.backend.get_words()))


class HealthCheckHandler(web.RequestHandler):
    """ Handler for the /status endpoint. """

    def get(self):
        self.write('All good!')


class Application(web.Application):
    """ Web Server class. """

    def __init__(self):
        root = 'static'
        backend = Backend()
        handlers = [
            (r'/search', SearchHandler, dict(backend=backend)),
            (r'/words', WordsHandler, dict(backend=backend)),
            (r'/status', HealthCheckHandler),
            # TODO(riccardo): manage to link this to the home directly.
            (r'/(.*)$', web.StaticFileHandler, {'path': root, 'default_filename': 'index.html'})

        ]
        web.Application.__init__(self, handlers)


logging.getLogger().setLevel(logging.INFO)
application = wsgi.WSGIAdapter(Application())

import re
from HTMLParser import HTMLParser, HTMLParseError


class OctoParser(HTMLParser):
    """ Subclass of the HTMLParser for extracting text from an HTML page."""

    def __init__(self):
        HTMLParser.__init__(self)
        self.buffer = []
        # This is used to ignore text we don't care about.
        self.ignore = False

    def handle_starttag(self, tag, attrs, **kwargs):
        if tag in {'script', 'style'}:
            self.ignore = True

    def handle_endtag(self, tag, **kwargs):
        if tag in {'script', 'style'}:
            self.ignore = False

    def handle_data(self, text):
        text = text.strip()
        if text and not self.ignore:
            # Strip multiple spaces.
            self.buffer.append(re.sub('\s+', ' ', text))

    def get_text(self):
        # Produce final output
        return ' '.join(self.buffer)


def extract_text(html):
    """ Extracts the text given an HTML page dump. """
    parser = OctoParser()
    try:
        parser.feed(html)
    except HTMLParseError:
        # TODO(riccardo): log here
        pass
    finally:
        parser.close()

    return parser.get_text()
